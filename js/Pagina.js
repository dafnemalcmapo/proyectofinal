var lastClickedImageId = null;



function expandImage(imageId, title, containerId) {
    var expandedText = document.getElementById(containerId);
    var img = document.getElementById(imageId);

    // Verificar si se hizo clic en la misma imagen nuevamente
    if (lastClickedImageId === imageId) {
        // Si es así, volvemos al estado original y ocultamos el texto
        img.style.transform = 'scale(1)';
        expandedText.style.display = 'none';
        lastClickedImageId = null;  // Restablecer el último ID de imagen
    } else {
        var patternText;
        var price;

        // Definir la información específica para cada patrón
        switch (imageId) {
            //casos para los patrones
            case 'patron1':
                patternText = 'Patrón de buho';
                price = '$150';
                break;
            case 'patron2':
                patternText = 'Patron de ositos frutales';  
                price = '$150'; 
                break;
            case 'patron3':
                patternText = 'Patron de ajolotitos';  
                price = '$100';  
                break;
            case 'patron4':
                patternText = 'Patron de Kirby durmiente';  
                price = '$200';  
                break;
            //casos para amigurumis
            case 'crochet1':
                patternText = 'Personalizables a eleccion';
                price = '$500';
                break;
            case 'crochet2':
                patternText = 'Vaquitas';
                price = '$400';
                break;
            case 'crochet3':
                patternText = 'Patitos con sombrero';
                price = '$350';
                break;
            case 'crochet4':
                patternText = 'Mini dinosaurios puffy';
                price = '$200';
                break;

            // Casos para estambres
            case 'estambres1':
                patternText = 'Estambre rooster 100% algodón';
                price = '$120 c/u';
                break;
            case 'estambres2':
                patternText = 'Estambre alize 90% algodón 10% acrílico';
                price = '$70 c/u';
                break;
            case 'estambres3':
                patternText = 'Estambre rowan tree acrílico';
                price = '$60 c/u';
                break;
            case 'estambres4':
                patternText = 'Estambre PMS perfecto para amigurumis y mantas';
                price = '$60 c/u';
                break;

            // Casos para ramos
            case 'ramo1':
                patternText = 'Bouquete de margaritas';
                price = '$600';
                break;
            case 'ramo2':
                patternText = 'Bouquete margaritas/tulipanes';
                price = '$400';
                break;
            case 'ramo3':
                patternText = 'Bouquete con amigurumi';
                price = '$650';
                break;
            case 'ramo4':
                patternText = 'Bouquete de flores diversas';
                price = '$800';
                break;

            default:
                patternText = 'Información no disponible';
                price = 'No disponible';
        }

        expandedText.innerHTML = '<p>' + patternText + '</p><p>Precio: ' + price + '</p>';
        
        // Mostrar el texto y cambiar el tamaño de la imagen
        expandedText.style.display = 'block';
        img.style.transform = 'scale(1.2)';

        // Actualizar el último ID de imagen
        lastClickedImageId = imageId;
    }
}
