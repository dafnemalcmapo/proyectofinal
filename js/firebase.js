
  // Import the functions you need from the SDKs you need
  import { initializeApp } from "https://www.gstatic.com/firebasejs/10.6.0/firebase-app.js";
  import { getAuth } from "https://www.gstatic.com/firebasejs/10.6.0/firebase-auth.js";
  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  // Your web app's Firebase configuration
  const firebaseConfig = {
    apiKey: "AIzaSyDomLfLG3By4d0mtEZX4ZmA59dPPm6Lli4",
    authDomain: "proyectofinal-217d3.firebaseapp.com",
    projectId: "proyectofinal-217d3",
    storageBucket: "proyectofinal-217d3.appspot.com",
    messagingSenderId: "414802275893",
    appId: "1:414802275893:web:f3d4d8f97ab42f906b8b7c"
  };

  // Initialize Firebase
  export const app = initializeApp(firebaseConfig);
  export const auth = getAuth(app);
