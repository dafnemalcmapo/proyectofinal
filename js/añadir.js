  // Import the functions you need from the SDKs you need
  import { initializeApp } from "https://www.gstatic.com/firebasejs/10.6.0/firebase-app.js";
  import { getAuth } from "https://www.gstatic.com/firebasejs/10.6.0/firebase-auth.js";
  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  // Your web app's Firebase configuration
  const firebaseConfig = {
    apiKey: "AIzaSyDomLfLG3By4d0mtEZX4ZmA59dPPm6Lli4",
    authDomain: "proyectofinal-217d3.firebaseapp.com",
    projectId: "proyectofinal-217d3",
    storageBucket: "proyectofinal-217d3.appspot.com",
    messagingSenderId: "414802275893",
    appId: "1:414802275893:web:f3d4d8f97ab42f906b8b7c"
  };

  // Initialize Firebase
  export const app = initializeApp(firebaseConfig);
  export const auth = getAuth(app);


// Obtiene la referencia al formulario y la sección de productos en la base de datos
const productForm = document.getElementById('product-form');
const productosRef = ref(database, 'Producto');

// Variable para controlar si el formulario está siendo procesado
let formProcessing = false;

// Evento de submit del formulario
productForm.addEventListener('submit', function (p) {
    p.preventDefault();

    // Si el formulario ya está siendo procesado, no hacer nada
    if (formProcessing) {
        return;
    }

    // Activar el indicador de procesamiento
    formProcessing = true;

    // Obtiene los valores de los campos de entrada
    const imgUrl = document.getElementById('img-url').value;
    const nombre = document.getElementById('nombre').value;
    const descripcion = document.getElementById('descripcion').value;

    // Añade un nuevo producto a la base de datos
    push(productosRef, {
        Imagen: imgUrl,
        Nombre: nombre,
        Descripcion: descripcion
    })
        .then(() => {
            // Limpia los campos del formulario después de agregar el producto
            document.getElementById('img-url').value = '';
            document.getElementById('nombre').value = '';
            document.getElementById('descripcion').value = '';

            // Cierra el modal
            const productModal = new bootstrap.Modal(document.getElementById('productModal'));
            productModal.hide();
        })
        .catch((error) => {
            console.error('Error al agregar el producto: ', error);
        })
        .finally(() => {
            // Desactivar el indicador de procesamiento
            formProcessing = false;
        });
});
